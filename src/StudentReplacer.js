
import { ApplicationService } from "@themost/common";
import { EdmMapping } from "@themost/data";
import { ModelClassLoaderStrategy, SchemaLoaderStrategy } from "@themost/data";
import { ProfileUtils } from "./ProfileUtils";

class Student {
    @EdmMapping.func("Me", "Student")
    static getMe(context) {
        if (context.user.profile) {
            // get student identifier
            const student = ProfileUtils.getStudentFromProfileString(context.user.profile);
            if (student) {
                return context.model('Student').where('user/name').notEqual(null)
                    .and('user/name').equal(context.user.name)
                    .and('id').equal(student)
                    .prepare();
            }
        }
        return context.model('Student').where('user/name').notEqual(null)
            .and('user/name').equal(context.user.name)
            .orderByDescending('inscriptionYear', 'inscriptionPeriod')
            .prepare();
    }
}

export class StudentReplacer extends ApplicationService {
    constructor(app) {
        super(app);
    }

    apply() {
        // get schema loader
        const schemaLoader = this.getApplication().getConfiguration().getStrategy(SchemaLoaderStrategy);
        // get model definition
        const model = schemaLoader.getModelDefinition('Student');
        // get model class
        const loader = this.getApplication().getConfiguration().getStrategy(ModelClassLoaderStrategy);
        const StudentBase = loader.resolve(model);
        // extend class
        StudentBase.getMe = Student.getMe;
    }
}
