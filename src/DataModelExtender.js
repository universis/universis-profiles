import { DataModel } from '@themost/data';
import { ProfileUtils } from './ProfileUtils';
import { ApplicationService } from "@themost/common";
// get super method
const superResolveMethod = DataModel.prototype.resolveMethod;
/**
 * @this DataModel
 */
const resolver = {
    /**
     * Gets student identifier based on the current context
     * @param {*} callback 
     * @returns 
     */
    student:function(callback) {
        const context = this.context;
        // if profile is defined
        if (context && context.user && context.user.profile) {
            // get student identifier (of profile)
            const student = ProfileUtils.getStudentFromProfileString(context.user.profile);
            // query student identifier of the current user
            // according to the specified profile
            let username = context.user.name;
            if (context.interactiveUser && context.interactiveUser.name) {
                username = context.interactiveUser.name;
            }
            return context.model('Student')
                .where('user/name').equal(username)
                .and('user/name').notEqual(null)
                .and('id').equal(student)
                .select('id').silent().value().then((student) => {
                    return callback(null, student || 0);  // student identifier or 0
                }).catch((err) => {
                    return callback(err);
                });
        }
        // otherwise call super method
        return superResolveMethod.bind(this)('student', [], callback);
    }
}

export class DataModelExtender extends ApplicationService {
    constructor(app) {
        super(app);
    }
    apply() {
        // override DataModel.resolveMethod()
        DataModel.prototype.resolveMethod = function(name, args, callback) {
            // try to find if resolver has a method with this name
            const resolverFunc = resolver[name];
            if (typeof resolverFunc === 'function') {
                // call resolver's method
                return resolverFunc.bind(this)(callback);
            }
            // otherwise call super methid
            return superResolveMethod.bind(this)(name, args, callback);
        };
    }
}

