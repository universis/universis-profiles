import { ApplicationService, HttpForbiddenError } from "@themost/common";
// eslint-disable-next-line no-unused-vars
import { ExpressDataApplication, ExpressDataContext, serviceRouter } from "@themost/express";
import { StudentReplacer } from './StudentReplacer';
import { DataModelExtender } from './DataModelExtender'
/**
 * @param {Router} parent
 * @param {Router} before
 * @param {Router} insert
 */
 function insertRouterBefore(parent, before, insert) {
    const beforeIndex = parent.stack.findIndex( (item) => {
        return item === before;
    });
    if (beforeIndex < 0) {
        throw new Error('Target router cannot be found in parent stack.');
    }
    const findIndex = parent.stack.findIndex( (item) => {
        return item === insert;
    });
    if (findIndex < 0) {
        throw new Error('Router to be inserted cannot be found in parent stack.');
    }
    // remove last router
    parent.stack.splice(findIndex, 1);
    // move up
    parent.stack.splice(beforeIndex, 0, insert);
}


export class ProfileService extends ApplicationService {
    constructor(app) {
        super(app);
        if (app && app.container) {
            // replace student model
            new StudentReplacer(app).apply();
            // extend DataModel class
            new DataModelExtender(app).apply();
            // add routes
            app.container.subscribe((container) => {
                if (container) {
                    // get server origin
                    const serverOrigin = this.getServerOrigin();
                    // find route
                    let before = serviceRouter.stack.find((item) => {
                        return item.route && item.route.path === '/:entitySet/:entitySetFunction';
                    });
                    // handle HEAD /api/students/me 
                    serviceRouter.head('/students/me', function getStudentProfiles(req, res, next) {
                        if (req.context.user == null) {
                            return next(new HttpForbiddenError());
                        }
                        /**
                         * get profile service
                         * @type {ProfileService}
                         */
                        const service = req.context.getApplication().getService(ProfileService);
                        return service.getStudentProfiles(req.context).then((results) => {
                                // format profiles
                                const items = results.map((item, index) => {
                                    return {
                                        rel: index === 0 ? 'canonical': 'alternate', // the first profile in the list is the canonical profile
                                        type: 'application/json', // the response is application/json
                                        title: item.title, // set profile title e.g. study program name
                                        description: item.description,
                                        profile: `urn:universis:student:profile:${item.id}` // and profile uri
                                    }
                                }).map((item) => {
                                    // build resource URI
                                    let resource = req.originalUrl;
                                    // if server origin is defined
                                    if (serverOrigin) {
                                        // set absolute URI
                                        resource = new URL(req.originalUrl, serverOrigin);
                                    }
                                    return `<${resource}>;rel="${item.rel}";type="${item.type}";profile="${item.profile}";title="${encodeURIComponent(item.title)}";description="${encodeURIComponent(item.description)}"`
                                });
                                if (items.length) {
                                    // add Link HTTP Header
                                    // see https://www.w3.org/TR/dx-prof-conneg/#http-listprofiles
                                    return res.set('Link', items.join(',')).status(200).send();
                                }
                                // otherwise return no content
                                return res.status(204).send();
                            }).catch((err) => {
                                return next(err);
                            });
                    });

                    // get last router
                    let insert = serviceRouter.stack[serviceRouter.stack.length - 1];
                    // insert (re-index) router
                    insertRouterBefore(serviceRouter, before, insert);
                    // get route again
                    before = serviceRouter.stack.find((item) => {
                        return item.route && item.route.path === '/:entitySet';
                    });
                    // handle Accept-Profile header
                    serviceRouter.use(function getStudentProfiles(req, res, next) {
                        const acceptProfile = req.get('Accept-Profile');
                        if (acceptProfile && acceptProfile.indexOf('urn:universis:student:profile:') === 0) {
                            if (req.context.user == null) {
                                return next(new HttpForbiddenError());
                            }
                            /**
                             * get profile service
                             * @type {ProfileService}
                             */
                            const service = req.context.getApplication().getService(ProfileService);
                            // get student profiles
                            return service.getStudentProfiles(req.context).then((results) => {
                                const findProfile = results.find((item) => {
                                    return `urn:universis:student:profile:${item.id}` === acceptProfile;
                                });
                                if (findProfile == null) {
                                    return next(new HttpForbiddenError('The specified profile is inaccessible.'));
                                }
                                // otherwise assign profile
                                Object.assign(req.context.user, {
                                    profile: `urn:universis:student:profile:${findProfile.id}`,
                                });
                                return next();
                            });
                        }
                        return next();
                    });
                    // get last route
                    insert = serviceRouter.stack[serviceRouter.stack.length - 1];
                    // insert (re-index) router
                    insertRouterBefore(serviceRouter, before, insert);

                }
            });
        }
    }

    /**
     * @typedef StudentProfileItem
     * @property {id} number
     * @property {string} title
     */

    /** 
     * @param {ExpressDataContext} context 
     * @returns Promise<StudentProfileItem[]>
     */
    getStudentProfiles(context) {
        return context.model('Student')
            .where('user/name').equal(context.user.name)
            .and('user/name').notEqual(null)
            .select('id', 'studyProgram/name as title', 'studyProgram/department/name as description')
            .orderByDescending('inscriptionYear', 'inscriptionPeriod')
            .silent()
            .getItems();
    }

    getServerOrigin() {
        return this.getApplication().getConfiguration().getSourceAt('settings/universis/api/origin');
    }

}