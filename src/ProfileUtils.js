import { LangUtils } from "@themost/common";

const StudentProfileRegExp = /^urn:universis:student:profile:(\d+)$/;

export class ProfileUtils {
    /**
     * Extracts student identifier from profile string
     * @param {string} str 
     * @returns {number|undefined}
     */
    static getStudentFromProfileString(str) {
        const matches = StudentProfileRegExp.exec(str);
        if (matches && matches.length) {
            return LangUtils.parseInt(matches[1]);
        }
    }
}