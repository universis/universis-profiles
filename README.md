# @universis/profiles

Negotiating profiles against universis api server 

This plugin of universis api server implements content negotiation as described in document https://www.w3.org/TR/dx-prof-conneg/#introduction

## Configuration

Register `@universis/profiles#ProfileService` in application services:

    "services": [
        ...,
        {
            "serviceType": "@universis/profiles#ProfileService"
        }
    ]

Configure CORS to expose `Link` header:

    "settings": {
        ...
        "cors": {
            "exposedHeaders": [
			"Link"
		    ],
		    "methods": "GET,HEAD,OPTIONS,PUT,PATCH,POST,DELETE"
        }
    }

## Get list of profiles

Get all the available student profiles for the context user:

    curl --location --head 'http://localhost:5001/api/students/me' \
    --header 'Authorization: Bearer a-valid-access-token'

This operation is important when a user is connected with more than one student records e.g. an undergraduate and a postgraduate program of studies.

The response header `Link` contains the available student profiles:

    <http://localhost:5001/api/students/me>;rel="canonical";type="application/json";profile="urn:universis:student:profile:33456111";title="Undergraduate study program A",
    <http://localhost:5001/api/students/me>;rel="alternate";type="application/json";profile="urn:universis:student:profile:22445523";title="Postgraduate study program B"

## Get resource for a specific profile

Use `Accept-Profile` request header to get a resource for a specific profile:

    curl --location --request GET 'http://localhost:5001/api/students/me' \
    --header 'Accept-Profile: urn:universis:student:profile:22445523' \
    --header 'Authorization: Bearer a-valid-access-token'

